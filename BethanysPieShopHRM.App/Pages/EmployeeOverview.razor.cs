﻿using BethanysPieShopHRM.App.Models;
using BethanysPieShopHRM.Shared.Domain;

namespace BethanysPieShopHRM.App.Pages
{
    public partial class EmployeeOverview
    {
        // Employee collection
        public List<Employee>? Employees { get; set; } = default!;
        private Employee? _selectedEmployee;

        // Initializing the data
        protected override void OnInitialized()
        {
            Employees = MockDataService.Employees;
        }

        // Function to show the Popup
        public void ShowQuickViewPopup(Employee selectedEmployee)
        {
            _selectedEmployee = selectedEmployee;
        }
    }
}
